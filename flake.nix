{
    inputs = {
        nixpkgs.url = "github:NixOS/nixpkgs";
        flake-utils.url = "github:numtide/flake-utils";
    };

    outputs = {self, nixpkgs, flake-utils}:
        flake-utils.lib.eachDefaultSystem (system:
        let pkgs = import nixpkgs { inherit system; };

            name = "poc";
            src = ./.;

            node-modules = pkgs.mkYarnPackage { inherit name src; };

            poc = pkgs.stdenv.mkDerivation {
                inherit name src;

                buildPhase = ''
                    ln -s ${node-modules}/libexec/${name}/node_modules node_modules
                    ln -s "${pkgs.google-fonts.override { fonts = [ "Inter" ]; } }/share/fonts/truetype/Inter[slnt,wght].ttf" src/app/Inter.ttf
                    ${pkgs.yarn}/bin/yarn build
                '';

                installPhase = ''
                    mkdir -p $out/bin

                    cp -r .next $out/.next
                    cp -r public $out/public
                    cp ./package.json $out/package.json
                    ln -s ${node-modules}/libexec/${name}/node_modules $out/node_modules

                    echo "#!${pkgs.stdenv.shell}" > $out/bin/${name}
                    echo "${pkgs.yarn}/bin/yarn start $out" >> $out/bin/${name}
                    chmod +x $out/bin/${name}
                '';
            };

            container = pkgs.dockerTools.buildImage {
                inherit name;
                config.Cmd = [ "${poc}/bin/${name}" ];
            };
        in {
            devShells.default = with pkgs; mkShell {
                packages = with pkgs; [git yarn];

                shellHook = ''
                    [[ -f ./src/app/Inter.ttf ]] && rm ./src/app/Inter.ttf
                    ln -s "${pkgs.google-fonts.override { fonts = [ "Inter" ]; } }/share/fonts/truetype/Inter[slnt,wght].ttf" src/app/Inter.ttf
                    alias l="${eza}/bin/eza -lAh"
                '';
            };

            packages = {
                default = poc;
                container = container;
            };
        });
}
